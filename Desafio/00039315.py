import sys
from os import listdir
from os.path import isfile, join
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap
from PyQt5 import QtGui

ALBUM = './steps/'


def image_extension(filename):
    extensions = ['jpg', 'jpeg', 'png']
    filename = filename.lower()
    extension = filename[-3:]
    especial_extension = filename[-4:]
    if extension in extensions or especial_extension in extensions:
        return True
    else:
        return False

def split_name(filename):
    name = filename.split(".")
    return name[0]



class DisplayImage(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self)
        self.parent = parent
        self.pixmap = QPixmap()
        self.label = QLabel(self)
        self.image_path = ''

    def update_image(self, path_to_image=''):
        self.image_path = path_to_image

        self.image_window()

    def image_window(self, event=None):
        windowSize = self.parent.size()
        window_height = windowSize.height()
        window_width = windowSize.width()

        image_max_height = window_height - 50
        image_max_width = window_width - 200

        self.pixmap = QPixmap(self.image_path)
        self.pixmap = self.pixmap.scaled(QSize(image_max_width, image_max_height), Qt.KeepAspectRatio, Qt.SmoothTransformation)

        self.label.setPixmap(self.pixmap)



class ImageFromAlbum(QWidget):
    def __init__(self, parent=None, album_path='', display_image=None):
        QWidget.__init__(self, parent=parent)
        self.display_image = display_image
        self.grid_layout = QGridLayout(self)
        self.grid_layout.setVerticalSpacing(30)

        self.grid_layout.setObjectName("grid_layout")


        files = [f for f in listdir(album_path) if isfile(join(album_path, f))]
        col_layout = 0
        welcome_image = ''




        for file_name in files:
            if image_extension(file_name) is False: continue
            img_label = QLabel()
            text_label = QLabel()
            img_label.setAlignment(Qt.AlignCenter)
            text_label.setAlignment(Qt.AlignCenter)
            file_path = album_path + file_name
            pixmap = QPixmap(file_path)
            pixmap = pixmap.scaled(QSize(100, 100),Qt.KeepAspectRatio,Qt.SmoothTransformation)
            img_label.setPixmap(pixmap)
            split_name = file_name.split(".")
            image_name = [f.split("_") for f in split_name]
            text_label.setText(image_name[0][1])
            img_label.mousePressEvent = lambda e, index=col_layout, file_path=file_path: self.selected_img(e, index, file_path)
            text_label.mousePressEvent = img_label.mousePressEvent
            vbox = QBoxLayout(QBoxLayout.TopToBottom)
            vbox.addWidget(img_label)
            vbox.addWidget(text_label)
            self.grid_layout.addLayout(vbox, col_layout, 0, Qt.AlignCenter)

            if col_layout == 0: welcome_image = file_path
            col_layout += 1

        self.selected_img(None, 0, welcome_image)

    def selected_img(self, event, index, img_file_path):
        for text_label_index in range(len(self.grid_layout)):
            text_label = self.grid_layout.itemAtPosition(text_label_index,0 ).itemAt(1).widget()

            text_label.setStyleSheet("background-color:None; border_radius: 25px;border: 1px solid grey")

        text_label_of_thumbnail = self.grid_layout.itemAtPosition(index, 0).itemAt(1).widget()
        text_label_of_thumbnail.setStyleSheet("background-color:grey; border-radius: 25px;border: 5px solid orange;")

        self.display_image.update_image(img_file_path)


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'MEF-3D'
        self.left = 0
        self.top = 0
        self.width = 900
        self.height = 700

        self.resizeEvent = lambda e : self.image_window(e)

        self.display_image = DisplayImage(self)
        self.image_file_selector = ImageFromAlbum( album_path=ALBUM, display_image=self.display_image)
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        scroll.setFixedWidth(200)
        nav = scroll
        nav.setWidget(self.image_file_selector)

        layout = QGridLayout(self)

        layout.addWidget(nav, 0, 0, Qt.AlignLeft)
        layout.addWidget(self.display_image.label, 0, 1, Qt.AlignRight)
        self.init_ui()

    def init_ui(self):
        self.setWindowIcon(QtGui.QIcon("icon.png"))
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setStyleSheet(open("./resources/style.qss", "r").read())
        self.show()

    def image_window(self, event):
        self.display_image.image_window(event)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
